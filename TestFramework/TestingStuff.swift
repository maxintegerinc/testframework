//
//  TestingStuff.swift
//  TestFramework
//
//  Created by Christopher Miller on 5/31/16.
//  Copyright © 2016 TweedleDee. All rights reserved.
//

import Foundation


public class MyTestClass {
    
    public init () {}
    
    public func myTestFunction() -> String {
        return "Hello from testing world"
    }
}